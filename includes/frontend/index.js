var handSize = 0;
var deck = [];
var availableCards = [];
var lifeTotal = 20;
var testDeck = [];
//var stack = []; // TODO: Eventually make this useful; the game "stack" where abilities are queued

var searchOptions = {
    source: availableCards
};

function getRotation(obj) {
    var matrix = obj.css("-webkit-transform") || obj.css("-moz-transform") || obj.css("-ms-transform") || obj.css("-o-transform") || obj.css("transform");

    var angle = 0;

    if (matrix !== 'none') {
        var values = matrix.split('(')[1].split(')')[0].split(',');
        var a = values[0];
        var b = values[1];

        angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
    }

    return (angle < 0) ? angle + 360 : angle;
}

function isScaled(obj) {
    return obj.getBoundingClientRect().width !== obj.offsetWidth;
}

function handleCardDrop(card, area, fromTop) {
    if (area.attr("id") === card.data("source")) {
        return;
    }

    if (card.data("source") === "deck") {
        if (fromTop) {
            deck.pop();
        } else {
            deck.splice(getCardIndex(card.attr("id")), 1);
        }

        flipCard(card);
    }

    if (area.attr("id") === "graveyard") {
        card.removeAttr("style").css({
            position: "absolute",
            top: "auto",
            left: "auto"
        }).appendTo(area);
    } else {
        card.removeAttr("style").css({
            top: "auto",
            left: "auto"
        }).appendTo(area);
    }

    if (area.attr("id") === "hand") {
        handSize++;
    } else if (card.data("source") === "hand") {
        handSize--;
    }

    card.data("source", card.parent("div").attr("id"));
}

function flipCard(card) {
    if (typeof jQuery === "function" && card instanceof jQuery) {
        card = card[0];
    }

    $(card).removeAttr("style").css({
        "transform": "rotateY(180deg) scaleX(-1)"
    });

    setTimeout(function () {
        var flippedImg = $(card).data("flip");
        var currentImg = $(card).find("img").attr("src");

        $(card).data("flip", currentImg);
        $(card).find("img").attr("src", flippedImg);
    }, 250);
}

function rotateCard(card) {
    if (typeof jQuery === "function" && card instanceof jQuery) {
        card = card[0];
    }

    var rotation = getRotation($(card));

    if (rotation === 0) {
        $(card).removeAttr("style").css({
            "transform": "rotateZ(90deg)"
        });
    } else {
        $(card).removeAttr("style").css({
            "transform": "rotateZ(0deg)"
        });
    }
}

function magnifyCard(card) {
    if (typeof jQuery === "function" && card instanceof jQuery) {
        card = card[0];
    }

    if (!isScaled(card)) {
        $(card).removeAttr("style").css({
            "transition": "all .2s ease-in-out",
            "transform": "scale(3)",
            "z-index": "1000"
        });
    } else {
        $(card).removeAttr("style");
    }
}

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    redrawDeck();

    return array;
}

function mulligan() {
    var mulliganSize = (handSize - 1 === 0) ? 1 : handSize - 1;

    $("#hand > div").each(function () {
        $(this).find("img").attr("src", "images/card_back.jpg"); // Force the image to change in the HTML

        deck.push($(this));

        $(this).remove();

        handSize--;
    });

    deck = shuffle(deck);

    for (var i = 0; i <= (mulliganSize - 1); i++) {
        var nextCard = deck[deck.length - 1];

        handleCardDrop(nextCard, $("#hand"), true);
    }
}

function searchDeck(cardName) {
    for (var i = 0; i < deck.length; i++) {
        var nextCard = deck[i];

        if (nextCard.attr("name") !== cardName)
            continue;

        return nextCard.attr("id");
    }
}

function getCardIndex(cardId) {
    for (var i = 0; i < deck.length; i++) {
        var nextCard = deck[i];

        if (nextCard.attr("id") === cardId) {
            return i;
        }
    }
}

function redrawDeck() {
    $("#deck").empty();

    $.each(deck, function(index, value) {
        $(value).appendTo("#deck").position({
            of: $("#deck"),
            my: "left top",
            at: "left top"
        }).draggable({
            containment: '#playArea',
            scroll: false,
            helper: "original",
            revert: true
        });
    });
}

function lookAt(numberOfCards) {
    // Modal popup that is like hand
    // Puts numberOfCards into modal
    // Checkboxes below cards to determine which ones you want to keep and which ones you want to put back
    // Input box next to check to determine where in the deck if the ability requires that (5th from bottom, etc)
    // Has buttons of "bottom", "top"
    // No number defaults to whichever button you press, random order
    // Other button: shuffle, to shuffle into deck

    // Alternatively: functions for each individual functionality; fire off based on tags on the card (new data- attribute)
    //                Will require MTGAPI potentially to determine effect
}

function updateLifeTotal(amount, isNegative) {
    if (isNegative) {
        lifeTotal = (lifeTotal - amount < 0) ? 0 : lifeTotal - amount;
    } else {
        lifeTotal = lifeTotal + amount;
    }

    $("#lifeTotal").progressbar("option", "value", lifeTotal);

    $("#lifeTotalNumber").html(lifeTotal);
}

function addCardToDeck(theCard) {
    var newEntry = [];

    newEntry.push(theCard.attr("id"));
    newEntry.push(theCard.attr("name"));
    newEntry.push(theCard.attr("data-source"));
    newEntry.push(theCard.attr("data-multiverseid"));
    newEntry.push(theCard.attr("data-flip"));
    newEntry.push(theCard.find("img").attr("src"));

    testDeck.push(newEntry);
}

// TODO: .toArray()
//       Make card DIV into proper array instead of saving the actual HTML
//       Will need entire system rework for things like mulligan and shuffle, etc.

// TODO: Token card creation function
//       Manual creation based on search function above solely for tokens

// TODO: Extra area for card management (intermediate area)
//       Use this to aid in cards similar to "draw top five of deck and only choose one to put into hand"
//       Does not count as actual hand; no limit; temporary
//       Allow to put cards here back into library

// TODO: Function to put cards on bottom of deck
//       Create a way to facilitate cards that say to put cards on the bottom of the deck
//       Should also allow for specific requirements like "5th card from the bottom" etc.

// TODO: A way to show the graveyard
//       Facilitate the player by allowing them to expand the graveyard and see their cards
//       To allow them to use cards like "return X from the graveyard"
//       Allow cards to be moved from graveyard again (may be unnecessary if graveyard stack bug is fixed)

// TODO: Extra area for exiled cards

// TODO: Counters
//       Positive counters
//       Negative counters

// TODO: Function to proliferate [QOL]
//       Only to make things easier
//       Unimportant; just listing for future creation

// TODO: Game functionality [!!!]
//       Listing out now, do later; NON-MULTIPLAYER FIRST
//       Mana cost requirements and tapping to cast a card (gray out cards that can't be cast based on total mana in pool); mana pool variable with breakdown of colors
//       Turn system
//       Way to determine card description and effects (manually tag cards in UI only if card description has the tag word in it?); pull from online database somehow (MTG API)?

// TODO: Flipped cards that are other cards

// TODO: Other game modes
//       Commander - Commander damage, etc.

$(document).ready(function () {
    Pace.on("done", function () {
        $("#loadingCover").hide();
    });

    $("#lifeTotal").progressbar({
        max: 20,
        value: 20,
        create: function () {
            $("#lifeTotalNumber").html(lifeTotal);
        },
        change: function () {
            if ($(this).progressbar("option", "value") <= 0) {
                $(this).progressbar("option", "value", 0);

                alert("YOU'RE DEAD");
            }
        }
    });

    $.getJSON("includes/deck.json", function(data) {
        var numCards = Object.keys(data.cards).length;
        var k = 0;

        for (var i = 0; i < numCards; i++) {
            var cardCount = data.cards[i].count;

            for (var j = 0; j < cardCount; j++) {
                var newCard = $('<div/>', {
                    id: "card_" + k,
                    name: data.cards[i].name,
                    class: "card"
                }).attr("data-source", "deck").attr("data-multiverseid", data.cards[i].multiverseid).attr("data-flip", "http://gatherer.wizards.com/Handlers/Image.ashx?multiverseid=" + data.cards[i].multiverseid + "&type=card");

                var cardArt = $("<img/>", {
                    src: "images/card_back.jpg",
                    alt: data.cards[i].name
                });

                if ($.inArray(data.cards[i].name, availableCards) === -1 ) {
                    availableCards.push(data.cards[i].name);
                }

                cardArt.appendTo(newCard);

                deck.push(newCard);
                testDeck.push(newCard.toArray());

                k++;
            }
        }

        deck = shuffle(deck);
    });

    $(document).on("click", "[id*='card_']", function (e) {
        if (e.shiftKey ) {
            if (Boolean($(this).data("flip")) && $(this).data("source") !== "deck") {
                flipCard($(this));
            }

            return;
        }

        if (e.ctrlKey) {
            if ($(this).data("source") !== "deck") {
                magnifyCard($(this));
            }

            return;
        }

        if ($(this).data("source") === "deck") {
            handleCardDrop($(this), $("#hand"), true);

            return;
        }

        if ($(this).data("source") !== "deck" && $(this).data("source") !== "hand" && $(this).data("source") !== "graveyard") {
            rotateCard($(this));
        }
    });

    $(document).on("focus.autocomplete", "input[id='deckSearch']", function () {
        if (!$(this).val()) {
            $(this).autocomplete(searchOptions).autocomplete("search");
        }
    });

    $(document).on("click", "button[id='applySearch']", function () {
        handleCardDrop($("#" + searchDeck($("#deckSearch").val())), $("#hand"), false);

        redrawDeck();
    });

    $("#shuffle").click(function () {
        deck = shuffle(deck);
    });

    $("#mulligan").click(function () {
        mulligan();
    });

    $("#enchantArts").droppable({
        accept: ".card",
        hoverClass: "hovered",
        drop: function (event, ui) {
            handleCardDrop(ui.draggable, $(this), true);
        }
    });

    $("#lands").droppable({
        accept: ".card",
        hoverClass: "hovered",
        drop: function (event, ui) {
            handleCardDrop(ui.draggable, $(this), true);
        }
    });

    $("#creatures").droppable({
        accept: ".card",
        hoverClass: "hovered",
        drop: function (event, ui) {
            handleCardDrop(ui.draggable, $(this), true);
        }
    });

    $("#hand").droppable({
        accept: ".card",
        hoverClass: "hovered",
        drop: function (event, ui) {
            handleCardDrop(ui.draggable, $(this), true);
        }
    });

    $("#graveyard").droppable({
        accept: ".card",
        hoverClass: "hovered",
        drop: function (event, ui) {
            handleCardDrop(ui.draggable, $(this), true);
        }
    });
});